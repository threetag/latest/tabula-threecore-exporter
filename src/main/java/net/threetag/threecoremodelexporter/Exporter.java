package net.threetag.threecoremodelexporter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import me.ichun.mods.ichunutil.client.gui.window.IWorkspace;
import me.ichun.mods.ichunutil.common.module.tabula.project.ProjectInfo;
import me.ichun.mods.ichunutil.common.module.tabula.project.components.CubeInfo;
import me.ichun.mods.tabula.client.core.ResourceHelper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Exporter extends me.ichun.mods.ichunutil.common.module.tabula.formats.types.Exporter {

    public static Gson GSON = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
    public StringBuilder errors;

    public Exporter() {
        super("export.threecore.name");
        errors = new StringBuilder();
    }

    @Override
    public boolean override(IWorkspace workspace) {
        workspace.addWindowOnTop(new WindowExportThreeCoreModel(workspace, workspace.width / 2 - 100, workspace.height / 2 - 80, 200, 160, 200, 160).putInMiddleOfScreen());
        return true;
    }

    @Override
    public boolean export(ProjectInfo projectInfo, Object... params) {
        Object filename = params[0];
        Object type = params[1];
        File file = new File(ResourceHelper.getExportsDir(), filename + ".json");
        List<CubeInfo> cubes = getStartCubes(projectInfo, type.toString());
        JsonObject json = new JsonObject();
        JsonArray array = new JsonArray();

        json.addProperty("type", type.toString());
        json.addProperty("scale", (float) params[2]);
        json.addProperty("texture_width", projectInfo.textureWidth);
        json.addProperty("texture_height", projectInfo.textureHeight);

        for (CubeInfo cube : cubes) {
            JsonObject cubeJson = serializeCube(projectInfo, cube, type.toString());
            array.add(cubeJson);
        }

        if (array.size() > 0)
            json.add("cubes", array);

        try {
            if (errors.length() == 0) {
                FileUtils.writeStringToFile(file, GSON.toJson(json));
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<CubeInfo> getStartCubes(ProjectInfo info, String type) {
        if (type.equalsIgnoreCase("threecore:biped")) {
            return info.getAllCubes().stream().filter(c -> {
                if (toBipedName(c.name) != null)
                    return false;

                CubeInfo bipedParent = getBipedParent(info, c.parentIdentifier);

                if (bipedParent == null) {
                    return c.parentIdentifier == null;
                } else {
                    return true;
                }
            }).collect(Collectors.toList());
        } else {
            return info.getAllCubes().stream().filter(c -> c.parentIdentifier == null).collect(Collectors.toList());
        }
    }

    public JsonObject serializeCube(ProjectInfo projectInfo, CubeInfo cubeInfo, String type) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("name", getFieldName(cubeInfo));
        if (cubeInfo.parentIdentifier != null) {
            String parent = "";
            if (type.equalsIgnoreCase("threecore:biped")) {
                CubeInfo bipedParent = getBipedParent(projectInfo, cubeInfo.parentIdentifier);
                if (bipedParent != null) {
                    parent = toBipedName(bipedParent.name);
                } else {
                    Object parentCube = projectInfo.getObjectByIdent(cubeInfo.parentIdentifier);
                    if (parentCube instanceof CubeInfo)
                        parent = ((CubeInfo) parentCube).name;
                }
            } else {
                Object parentCube = projectInfo.getObjectByIdent(cubeInfo.parentIdentifier);
                if (parentCube instanceof CubeInfo)
                    parent = ((CubeInfo) parentCube).name;
            }
            if (!parent.isEmpty())
                jsonObject.addProperty("parent", parent);
        }
        jsonObject.addProperty("scale", cubeInfo.mcScale);
        jsonObject.addProperty("mirror", cubeInfo.txMirror);

        JsonArray textureOffset = new JsonArray();
        textureOffset.add(cubeInfo.txOffset[0]);
        textureOffset.add(cubeInfo.txOffset[1]);
        jsonObject.add("texture_offset", textureOffset);

        JsonArray offset = new JsonArray();
        offset.add(cubeInfo.offset[0]);
        offset.add(cubeInfo.offset[1]);
        offset.add(cubeInfo.offset[2]);
        jsonObject.add("offset", offset);

        JsonArray rotationPoint = new JsonArray();
        rotationPoint.add(cubeInfo.position[0]);
        rotationPoint.add(cubeInfo.position[1]);
        rotationPoint.add(cubeInfo.position[2]);
        jsonObject.add("rotation_point", rotationPoint);

        JsonArray size = new JsonArray();
        size.add(cubeInfo.dimensions[0]);
        size.add(cubeInfo.dimensions[1]);
        size.add(cubeInfo.dimensions[2]);
        jsonObject.add("size", size);

        JsonArray rotation = new JsonArray();
        rotation.add(cubeInfo.rotation[0]);
        rotation.add(cubeInfo.rotation[1]);
        rotation.add(cubeInfo.rotation[2]);
        jsonObject.add("rotation", rotation);

        if (cubeInfo.getChildren().size() > 0) {
            JsonArray children = new JsonArray();
            for (CubeInfo childrenCube : cubeInfo.getChildren()) {
                children.add(serializeCube(projectInfo, childrenCube, type));
            }
            jsonObject.add("children", children);
        }

        return jsonObject;
    }

    public CubeInfo getBipedParent(ProjectInfo projectInfo, String id) {
        Object object = projectInfo.getObjectByIdent(id);
        if (object instanceof CubeInfo) {
            if (toBipedName(((CubeInfo) object).name) == null) {
                return null;
            } else {
                return (CubeInfo) object;
            }
        }
        return null;
    }

    public String toBipedName(String cubeName) {
        if (cubeName.equalsIgnoreCase("bipedHead") || cubeName.equalsIgnoreCase("field_78116_c"))
            return "head";
        else if (cubeName.equalsIgnoreCase("bipedHeadwear") || cubeName.equalsIgnoreCase("field_178720_f"))
            return "head_overlay";
        else if (cubeName.equalsIgnoreCase("bipedBody") || cubeName.equalsIgnoreCase("field_78115_e"))
            return "chest";
        else if (cubeName.equalsIgnoreCase("bipedBodyWear") || cubeName.equalsIgnoreCase("field_178730_v"))
            return "chest_overlay";
        else if (cubeName.equalsIgnoreCase("bipedRightArm") || cubeName.equalsIgnoreCase("field_178723_h"))
            return "right_arm";
        else if (cubeName.equalsIgnoreCase("bipedRightArmwear") || cubeName.equalsIgnoreCase("field_178732_b"))
            return "right_arm_overlay";
        else if (cubeName.equalsIgnoreCase("bipedLeftArm") || cubeName.equalsIgnoreCase("field_178724_i"))
            return "left_arm";
        else if (cubeName.equalsIgnoreCase("bipedLeftArmwear") || cubeName.equalsIgnoreCase("field_178734_a"))
            return "left_arm_overlay";
        else if (cubeName.equalsIgnoreCase("bipedRightLeg") || cubeName.equalsIgnoreCase("field_178721_j"))
            return "right_leg";
        else if (cubeName.equalsIgnoreCase("bipedRightLegwear") || cubeName.equalsIgnoreCase("field_178731_d"))
            return "right_leg_overlay";
        else if (cubeName.equalsIgnoreCase("bipedLeftLeg") || cubeName.equalsIgnoreCase("field_178722_k"))
            return "left_leg";
        else if (cubeName.equalsIgnoreCase("bipedLeftLegwear") || cubeName.equalsIgnoreCase("field_178733_c"))
            return "left_leg_overlay";
        return null;
    }

    public String getFieldName(CubeInfo cube) {
        String name = cube.name.replaceAll("[^A-Za-z0-9_$]", "");
        return name;
    }

    @Override
    public boolean localizable() {
        return true;
    }
}
