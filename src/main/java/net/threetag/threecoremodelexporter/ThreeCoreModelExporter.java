package net.threetag.threecoremodelexporter;

import me.ichun.mods.tabula.client.export.ExportList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = ThreeCoreModelExporter.MODID, name = ThreeCoreModelExporter.NAME, version = ThreeCoreModelExporter.VERSION)
public class ThreeCoreModelExporter {

    public static final String MODID = "tabulathreecoreexporter";
    public static final String NAME = "Tabula ThreeCore Exporter";
    public static final String VERSION = "1.12.2-1.0.0";

    private static Logger LOGGER;
    public static Exporter EXPORTER;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        LOGGER = event.getModLog();

        ExportList.exportTypes.add(EXPORTER = new Exporter());
    }
}
