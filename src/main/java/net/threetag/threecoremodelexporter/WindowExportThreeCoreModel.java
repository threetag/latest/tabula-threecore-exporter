package net.threetag.threecoremodelexporter;

import me.ichun.mods.ichunutil.client.gui.window.IWorkspace;
import me.ichun.mods.ichunutil.client.gui.window.Window;
import me.ichun.mods.ichunutil.client.gui.window.element.*;
import me.ichun.mods.ichunutil.common.module.tabula.project.ProjectInfo;
import me.ichun.mods.tabula.client.gui.GuiWorkspace;
import me.ichun.mods.tabula.client.gui.Theme;
import me.ichun.mods.tabula.client.gui.window.WindowExportBlockJsonFailed;
import net.minecraft.util.text.translation.I18n;

import java.util.Arrays;
import java.util.List;

public class WindowExportThreeCoreModel extends Window {

    public static final List<String> EXPORT_TYPES = Arrays.asList("threecore:default", "threecore:biped");

    public WindowExportThreeCoreModel(IWorkspace parent, int x, int y, int w, int h, int minW, int minH) {
        super(parent, x, y, w, h, minW, minH, "export.threecore.name", true);
        ElementSelector selector = new ElementSelector(this, 10, 30, this.width - 20, 12, 1, I18n.translateToLocal("export.threecore.type"), EXPORT_TYPES.get(0));
        EXPORT_TYPES.forEach(s -> selector.choices.put(s, s));
        elements.add(selector);

        String suggestedName = ((GuiWorkspace) workspace).projectManager.projects.get(((GuiWorkspace) workspace).projectManager.selectedProject).modelName.toLowerCase().replaceAll(" ", "_");
        ElementTextInput fileName = new ElementTextInput(this, 10, 65, this.width - 20, 12, 2, I18n.translateToLocal("export.threecore.filename"));
        fileName.textField.setText(suggestedName);
        elements.add(fileName);

        elements.add(new ElementNumberInput(this, 10, 100, 80, 12, 5, I18n.translateToLocal("export.threecore.biped_scale"), 1, true, 0F, 32F));

        this.elements.add(new ElementButton(this, this.width - 140, this.height - 30, 60, 16, 100, false, 1, 1, "element.button.ok"));
        this.elements.add(new ElementButton(this, this.width - 70, this.height - 30, 60, 16, 0, false, 1, 1, "element.button.cancel"));
    }

    @Override
    public void elementTriggered(Element element) {
        if (element.id == 0) {
            workspace.removeWindow(this, true);
        } else if (element.id == 100) {
            ProjectInfo info = ((GuiWorkspace) workspace).projectManager.projects.get(((GuiWorkspace) workspace).projectManager.selectedProject);

            String type = "";
            String fileName = "";
            float scale = 0F;

            for (Element element1 : elements) {
                if (element1 instanceof ElementTextInput) {
                    ElementTextInput text = (ElementTextInput) element1;
                    if (text.id == 2) {
                        fileName = text.textField.getText();
                    }
                } else if (element1 instanceof ElementSelector) {
                    if (element1.id == 1) {
                        type = ((ElementSelector) element1).selected;
                    }
                } else if (element1 instanceof ElementNumberInput) {
                    try {
                        if (element1.id == 5 && ((ElementNumberInput) element1).textFields.size() > 0 && ((ElementNumberInput) element1).textFields.get(0) != null) {
                            scale = Float.parseFloat(((ElementNumberInput) element1).textFields.get(0).getText());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if (type.isEmpty() || fileName.isEmpty())
                return;

            if (!ThreeCoreModelExporter.EXPORTER.export(info, fileName, type, scale)) {
                WindowExportBlockJsonFailed errorDialog = new WindowExportBlockJsonFailed(workspace, 0, 0, 180, 80, 180, 80);
                errorDialog.setInfoText(ThreeCoreModelExporter.EXPORTER.errors.toString());
                errorDialog.putInMiddleOfScreen();
                workspace.addWindowOnTop(errorDialog);
            }

            workspace.removeWindow(this, true);
        }
    }

    @Override
    public void draw(int mouseX, int mouseY) {
        super.draw(mouseX, mouseY);

        if (!minimized) {
            workspace.getFontRenderer().drawString(I18n.translateToLocal("export.threecore.type"), posX + 11, posY + 20, Theme.getAsHex(workspace.currentTheme.font), false);
            workspace.getFontRenderer().drawString(I18n.translateToLocal("export.threecore.filename"), posX + 11, posY + 55, Theme.getAsHex(workspace.currentTheme.font), false);
            workspace.getFontRenderer().drawString(I18n.translateToLocal("export.threecore.biped_scale"), posX + 11, posY + 90, Theme.getAsHex(workspace.currentTheme.font), false);
        }
    }
}
